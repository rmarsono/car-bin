<?php
$email_1 = "rendy@bubblefish.com.au";
$email_2 = "avi@theracingsnail.com.au";

$subject = "Message from Car-Bin Web Form";

//teddy added 2008/05/07:
//flash passed the variables with the htmlspecial chars encoded to &amp; like that.
//so we need to decode it again back to the original.
//but still strip the tags for security.
$headers = "From:	" . strip_tags(htmlspecialchars_decode($_POST["email"])) . "\n";
$message = "Message:	" . strip_tags(htmlspecialchars_decode($_POST["message"])) . "\n";
$message .= "Sender name:	" . strip_tags(htmlspecialchars_decode($_POST["name"])) . "\n";
$message .= "Email address:	" . strip_tags(htmlspecialchars_decode($_POST["email"])) . "\n";
$message .= "Phone Number:	" . strip_tags(htmlspecialchars_decode($_POST["number"])) . "\n";
$message .= "This person is from:	" . strip_tags(htmlspecialchars_decode($_POST["company"])) . "\n";
$message .= "How this person found Car-Bin:	" . strip_tags(htmlspecialchars_decode($_POST["how"])) . "\n";

mail($email_1, $subject, $message, $headers);
mail($email_2, $subject, $message, $headers);

?>
